package com.desafio.springboot.app.recuperarpassword;

import com.desafio.springboot.app.recuperarpassword.web.rest.UserController;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class DesafioServicioRecuperarPasswordApplicationTests {

	@Autowired
	private  UserController controller;

	
	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}

}
