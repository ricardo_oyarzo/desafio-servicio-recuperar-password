package com.desafio.springboot.app.recuperarpassword.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import com.desafio.springboot.app.recuperarpassword.model.User;
import com.desafio.springboot.app.recuperarpassword.repository.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements IUserService {
    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
    private static final long EXPIRE_TOKEN_AFTER_MINUTES = 30;

    @Autowired
    private UserRepository userRepository;

    @Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Funcion para ejecutar al proceso de "Olvide mi contraseña"
     * 
     * @param email
     * @return Token o 'Email inválido'
     */
    @Override
    public String olvidoPassword(String email) {
        Optional<User> userOptional = Optional.ofNullable(this.userRepository.findByEmail(email));
                
        if(!userOptional.isPresent()) {
            return "Email inválido";
        } else {
            log.debug("usuario: user {}", userOptional.get().getEmail());
        }
        User user = userOptional.get();
        user.setToken(generarTokenUnico());
        user.setTokenFechaCreacion(LocalDateTime.now());
        user = userRepository.save(user);

        return user.getToken();
    }

    /**
     * Función para generar un reinicio de password si es que corresponde
     *  
     * @param token
     * @param password
     * @return Texto explicativo
     */
    @Override
    public String reiniciarPassword(String token, String password) {
        Optional<User> userOptional = Optional.ofNullable(userRepository.findByToken(token));

        if(!userOptional.isPresent()) {
            return "Token invalido";
        }

        LocalDateTime tokenFechaCreacion = userOptional.get().getTokenFechaCreacion();

        if(tokenExpirado(tokenFechaCreacion)) {
            return "Token expirado";
        }

        
        User user = userOptional.get();
        // Encriptar Password
        user.setPassword(bCryptPasswordEncoder.encode(password));
        // BCryptPasswordEncoder
        user.setToken(null);
        user.setTokenFechaCreacion(null);

        userRepository.save(user);

        return "Password cambiada exitosamente";
    }

    /**
	 * Genera token único. (se pueden incorporar mas parametros)
	 * 
	 * @return token único
	 */
    @Override
    public String generarTokenUnico() {
        StringBuilder token = new StringBuilder();

		return token.append(UUID.randomUUID().toString())
				.append(UUID.randomUUID().toString()).toString();
    }

    /**
	 * Verifica si el token a expirado o no.
	 * 
	 * @param tokenFechaCreacion
	 * @return true or false
	 */
    @Override
    public boolean tokenExpirado(LocalDateTime tokenFechaCreacion) {
        LocalDateTime now = LocalDateTime.now();
		Duration diff = Duration.between(tokenFechaCreacion, now);

		return diff.toMinutes() >= EXPIRE_TOKEN_AFTER_MINUTES;
    }
    
}
