package com.desafio.springboot.app.recuperarpassword.service;

import java.time.LocalDateTime;

public interface IUserService {
    public String olvidoPassword(String email);
    public String reiniciarPassword(String token, String password);
    String generarTokenUnico();
    boolean tokenExpirado(final LocalDateTime tokenFechaCreacion);
}
