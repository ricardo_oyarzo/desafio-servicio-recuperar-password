package com.desafio.springboot.app.recuperarpassword.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service("emailService")
public class EmailServiceImpl implements IEmailService {

    @Autowired
    private JavaMailSender mailSender;
    
    @Override
    @Async
    public void enviarEmail(SimpleMailMessage email) {
        mailSender.send(email);
    }
    
}
