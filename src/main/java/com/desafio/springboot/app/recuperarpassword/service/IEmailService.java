package com.desafio.springboot.app.recuperarpassword.service;

import org.springframework.mail.SimpleMailMessage;

public interface IEmailService {

    public void enviarEmail(SimpleMailMessage email);
}
