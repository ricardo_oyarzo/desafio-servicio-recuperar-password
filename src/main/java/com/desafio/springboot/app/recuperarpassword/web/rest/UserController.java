package com.desafio.springboot.app.recuperarpassword.web.rest;

import javax.servlet.http.HttpServletRequest;

import com.desafio.springboot.app.recuperarpassword.service.IEmailService;
import com.desafio.springboot.app.recuperarpassword.service.IUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class UserController {
    
    @Autowired
    private IUserService userService;

    @Autowired
    private IEmailService emailService;
    
    @Autowired
    private HttpServletRequest request;
    
    @Value("${spring.mail.from}")
    private String mailFrom;

    @PostMapping("/forgot-password")
    public String postForgotPassword(@RequestParam String email) {
        String response = userService.olvidoPassword(email);

        if(!response.startsWith("Email")) {
            String rutaservicio = this.request.getScheme() + "://" + request.getServerName();
            response = rutaservicio + ":3000/cambiarpassword/" + response;

            // Proceso de envío de correo
            SimpleMailMessage reinicioPasswordEmail = new SimpleMailMessage();
            reinicioPasswordEmail.setFrom(mailFrom);
            reinicioPasswordEmail.setTo(email);
            reinicioPasswordEmail.setSubject("Reinicio de password");
            reinicioPasswordEmail.setText("Para reiniciar su password, haga click en el siguiente link:\n" + response);
            emailService.enviarEmail(reinicioPasswordEmail);
        }

        return response;
    }

    @RequestMapping("/reset-password")
    public String postResetPassword(@RequestParam(value = "token") String token, @RequestParam(value = "password") String password) {
        return userService.reiniciarPassword(token, password);
    }
}
