package com.desafio.springboot.app.recuperarpassword.repository;

import com.desafio.springboot.app.recuperarpassword.model.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    
    User findByEmail(String email);

    User findByToken(String token);
}
